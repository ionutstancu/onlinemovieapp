package mind.geek.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.extern.log4j.Log4j2;
import mind.geek.cacheLoader.CacheLoader;
import mind.geek.model.Director;
import mind.geek.model.MovieDetail;
import mind.geek.service.implementation.MovieDetailServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

@SpringBootTest
@Log4j2
public class MovieDetailServiceTest {

    @InjectMocks
    private MovieDetailServiceImpl movieDetailServiceImpl;

    @Mock
    private CacheLoader cacheLoader;

    private List<MovieDetail> movieDetailList = new ArrayList<>();


    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        MovieDetail movieDetail1 = new MovieDetail();
        MovieDetail movieDetail2 = new MovieDetail();

        List<String> genresList1 = new ArrayList<>();
        genresList1.add("Romance");
        genresList1.add("Comedy");

        List<Director> directorsList1 = new ArrayList<>();
        Director director1 = new Director();
        director1.setName("director1");
        directorsList1.add(director1);

        movieDetail1.setId("id1");
        movieDetail1.setDuration(180);
        movieDetail1.setGenres(genresList1);
        movieDetail1.setRating(1);
        movieDetail1.setYear(2019);
        movieDetail1.setDirectors(directorsList1);

        List<String> genresList2 = new ArrayList<>();
        genresList2.add("Thriller");
        genresList2.add("Romance");

        List<Director> directorsList2 = new ArrayList<>();
        Director director2 = new Director();
        director2.setName("director2");
        directorsList2.add(director2);

        movieDetail2.setId("id2");
        movieDetail2.setDuration(120);
        movieDetail2.setGenres(genresList2);
        movieDetail2.setRating(2);
        movieDetail2.setYear(2020);
        movieDetail2.setDirectors(directorsList2);

        movieDetailList.add(movieDetail1);
        movieDetailList.add(movieDetail2);

        when(cacheLoader.getMovieDetailList()).thenReturn(movieDetailList);
    }

    /**
     * This method is used to retrieve all data movie details
     */
    @Test
    public void getAllMoviesDetails() throws JsonProcessingException {
        String movieDetails = movieDetailServiceImpl.getAllMoviesDetails();

        assertTrue(movieDetails.contains(movieDetailList.get(0).getId()));
        assertTrue(movieDetails.contains(movieDetailList.get(0).getDuration().toString()));
    }

    /**
     * This method is used to retrieve all data movie details filtered by genres
     */
    @Test
    public void getListMovieDetailsByGenres() {
        List<String> list = new ArrayList<>();
        list.add("Thriller");
        List<MovieDetail> movieDetailList = movieDetailServiceImpl.getListMovieDetailsByGenres(list);

        assertEquals(1, movieDetailList.size());
    }

    /**
     * This method is used to retrieve all data movie details filtered by director names
     */
    @Test
    public void getListMovieDetailsByDirectors() {
        List<String> list = new ArrayList<>();
        list.add("director2");
        List<MovieDetail> movieDetailList = movieDetailServiceImpl.getListMovieDetailsByDirectors(list);

        assertEquals(1, movieDetailList.size());
    }

    /**
     * This method is used to retrieve all data movie details filtered by rating
     */
    @Test
    public void getListMovieDetailsByRating() {
        List<MovieDetail> movieDetailList = movieDetailServiceImpl.getListMovieDetailsByRating(2);

        assertEquals(1, movieDetailList.size());
    }

    /**
     * This method is used to retrieve all data movie details filtered by year
     */
    @Test
    public void getListMovieDetailsByYear() {
        List<MovieDetail> movieDetailList = movieDetailServiceImpl.getListMovieDetailsByYear(2019);

        assertEquals(1, movieDetailList.size());
    }

    /**
     * This method is used to retrieve all data movie details filtered by year
     */
    @Test
    public void getListMovieDetailsByYearBiggerThan() {
        List<MovieDetail> movieDetailList = movieDetailServiceImpl.getListMovieDetailsByYearBiggerThan(2019);

        assertEquals(1, movieDetailList.size());
    }
}