package mind.geek.cacheLoader;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import lombok.Getter;
import lombok.extern.log4j.Log4j2;
import mind.geek.exceptionHandling.BlobResponseHttpError;
import mind.geek.helper.JAXBUtils;
import mind.geek.model.CardImage;
import mind.geek.model.MovieDetail;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.List;

import static mind.geek.constants.Constants.*;


/**
 * This class was created for pre-building 'MovieDetail' list
 */

@Component
@Getter
@Log4j2
public class CacheLoader {

    private List<MovieDetail> movieDetailList;

    /**
     * This method is used to retrieve some data from a given url
     * The method use 'Cacheable' annotation to ensure the efficiency
     */
    @PostConstruct
    @Cacheable(CACHED_MOVIE)
    public void setMovieDetailListFromAGivenUrl() {
        RestTemplate restTemplate = new RestTemplate();
        String jsonArrayString = restTemplate.getForObject(movieUrl, String.class);

        /*Rename jsonField 'class' due to java bean names*/
        if (null != jsonArrayString) {
            jsonArrayString = jsonArrayString.replaceAll("class", "movieClass");
        }

        Type collectionType = new TypeToken<List<MovieDetail>>() {
        }.getType();

        movieDetailList = new Gson().fromJson(jsonArrayString, collectionType);

        if (null == movieDetailList) {
            return;
        }

        try {
            setMovieImageAsBytes(restTemplate);
        } catch (JAXBException e) {
            log.error(UNMARSHALL_ERROR, e);
        }
    }

    /**
     * This method is used to set for each assets of movies the corresponding images as bytes
     *
     * @param restTemplate - http rest template
     * @throws JAXBException - see {@link JAXBException}
     */
    private void setMovieImageAsBytes(RestTemplate restTemplate) throws JAXBException {
        for (MovieDetail movieDetail : movieDetailList) {
            try {
                List<CardImage> cardImageList = movieDetail.getCardImages();
                setImageBytes(restTemplate, cardImageList);

                List<CardImage> keyArtImages = movieDetail.getKeyArtImages();
                setImageBytes(restTemplate, keyArtImages);
            } catch (HttpClientErrorException e) {
                JAXBElement<BlobResponseHttpError> jaxbElement = JAXBUtils.toObject(e.getResponseBodyAsString(), BlobResponseHttpError.class);
                log.error(jaxbElement.getValue().getMessage());
            }
        }
    }

    /**
     * This method is used to set image bytes on each corresponding object (asset)
     *
     * @param restTemplate rest http template
     * @param imageList    the list of images
     */
    private void setImageBytes(RestTemplate restTemplate, List<CardImage> imageList) {
        for (CardImage image : imageList) {
            String imageUrl = image.getUrl();
            byte[] imageAsByte = restTemplate.getForObject(imageUrl, byte[].class);
            image.setImageBytes(imageAsByte);
        }
    }
}
