package mind.geek.helper;

import mind.geek.exceptionHandling.BlobResponseHttpError;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;
import java.io.StringReader;

/**
 * Utility class created for parsing error
 *
 */
public class JAXBUtils {

    /**
     * This method was created to parse a xml string to a generic object
     *
     * @param xmlString xml in string format
     * @param tClass    the given object
     * @throws JAXBException - see {@link JAXBException}
     *
     * @return the method returns a JAXBElement
     */
    public static <T> JAXBElement<BlobResponseHttpError> toObject(String xmlString, Class<T> tClass) throws JAXBException {
        /*Precheck condition*/
        if (null == xmlString) {
            return null;
        }
        /*The message contained a special character, therefore should be replaced*/
        xmlString = xmlString.replaceAll("[\uFEFF-\uFFFF]", "");
        JAXBContext jaxbContext = JAXBContext.newInstance(tClass);
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();

        return (JAXBElement<BlobResponseHttpError>) unmarshaller.unmarshal(new StreamSource(new StringReader(xmlString)), tClass);
    }
}
