package mind.geek.helper;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

/**
 * Utility class used for conversion from json to objects
 *
 * */
public class JsonUtils {

    /**
     * This method is used to transform object into a string with json format
     *
     * @param object - the given object
     * @return json object
     * @throws JsonProcessingException - see {@link JsonProcessingException}
     * */
    public static String toJsonString(Object object) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(object);
    }

    /**
     * This method is used to transform a json into a corresponding object
     *
     * @param jsonString the json as string
     * @throws IOException - see {@link IOException}
     * @return the method returns the object
     * */
    public static Object toObject(String jsonString) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(jsonString, Object.class);
    }
}
