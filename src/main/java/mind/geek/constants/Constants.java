package mind.geek.constants;

public interface Constants {

    /*Cache loader constants*/
    String CACHED_MOVIE = "movieInformation";

    /*Json url*/
    String movieUrl = "https://mgtechtest.blob.core.windows.net/files/showcase.json";

    /*Movie endpoints*/
    String MOVIES = "/movies";
    String ALL_MOVIE_DETAILS = "/all";
    String GET_MOVIE_DETAILS_BY_GENRES = "/byGenres/{genresList}";
    String GET_MOVIE_DETAILS_BY_DIRECTOR_NAMES = "/byDirectors/{directorsList}";
    String GET_MOVIE_DETAILS_BY_RATING = "/byRating/{rating}";
    String GET_MOVIE_DETAILS_BY_YEAR = "/byYear/{year}";
    String GET_MOVIE_DETAILS_BY_YEAR_BIGGER_THAN = "/byYearBiggerThan/{year}";

    /*Error handing messages*/
    String UNMARSHALL_ERROR = "An error has occurred while deserializing the object from jaxb utils class!";
    String JSON_UNPROCESSED_EXCEPTION = "An error has occurred while processing the json!";
}
