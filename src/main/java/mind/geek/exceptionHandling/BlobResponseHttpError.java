package mind.geek.exceptionHandling;

import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Getter
@Setter
@Component
@XmlRootElement(name = "Error")
@XmlAccessorType(XmlAccessType.FIELD)
public class BlobResponseHttpError implements Serializable {
    private static final long serialVersionUID = -1392845623456872905L;

    @XmlElement(name = "Code")
    private String code;
    @XmlElement(name = "Message")
    private String message;
}
