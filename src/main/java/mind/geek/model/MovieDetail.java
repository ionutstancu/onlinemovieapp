package mind.geek.model;

import com.fasterxml.jackson.annotation.*;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@Component
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MovieDetail implements Serializable {
    private static final long serialVersionUID = 6297857057641632415L;

    private String body;
    private List<CardImage> cardImages;
    private List<Cast> cast;
    private String cert;
    private String movieClass;
    private List<Director> directors;
    private Integer duration;
    private List<String> genres;
    private String headline;
    private String id;
    private List<CardImage> keyArtImages;
    private String lastUpdated;
    private String qoute;
    private Integer rating;
    private String reviewAuthor;
    private String skyGoId;
    private String skyGoUrl;
    private String sum;
    private String synopsis;
    private String url;
    private List<Video> videos;
    private ViewingWindow viewingWindow;
    private Integer year;
}
