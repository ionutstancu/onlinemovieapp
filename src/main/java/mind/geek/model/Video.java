package mind.geek.model;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
public class Video implements Serializable {
    private static final long serialVersionUID = -8493472146553999829L;

    private String title;
    private String type;
    private String url;
    private List<Alternative> alternatives;
}
