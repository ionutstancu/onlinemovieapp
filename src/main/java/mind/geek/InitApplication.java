package mind.geek;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication(scanBasePackages = "mind.geek")
@EnableCaching
public class InitApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(InitApplication.class, args);
    }
}
