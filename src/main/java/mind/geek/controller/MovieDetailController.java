package mind.geek.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.extern.log4j.Log4j2;
import mind.geek.constants.Constants;
import mind.geek.model.MovieDetail;
import mind.geek.service.MovieDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static mind.geek.constants.Constants.*;

@RestController
@RequestMapping(Constants.MOVIES)
@Log4j2
public class MovieDetailController {

    @Autowired
    private MovieDetailService movieDetailService;

    /**
     * This method is used to retrieve all data movie details
     *
     * @return movie details json as string
     */
    @GetMapping(path = ALL_MOVIE_DETAILS, produces = MediaType.APPLICATION_JSON_VALUE)
    public String getAllMoviesDetails() {
        String jsonMovieList = null;
        try {
            jsonMovieList = movieDetailService.getAllMoviesDetails();
        } catch (JsonProcessingException e) {
            log.error(JSON_UNPROCESSED_EXCEPTION, e);
        }
        return jsonMovieList;
    }

    /**
     * This method is used to retrieve all data movie details filtered by genres
     *
     * @param genresList the list of genres by which the filter will be made
     * @return movie details json as string
     */
    @GetMapping(path = GET_MOVIE_DETAILS_BY_GENRES, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<MovieDetail> getListMovieDetailsByGenres(@PathVariable final List<String> genresList) {
        return movieDetailService.getListMovieDetailsByGenres(genresList);
    }

    /**
     * This method is used to retrieve all data movie details filtered by director names
     *
     * @param directorsList the list of director names by which the filter will be made
     * @return movie details json as string
     */
    @GetMapping(path = GET_MOVIE_DETAILS_BY_DIRECTOR_NAMES, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<MovieDetail> getListMovieDetailsByDirectors(@PathVariable final List<String> directorsList) {
        return movieDetailService.getListMovieDetailsByDirectors(directorsList);
    }

    /**
     * This method is used to retrieve all data movie details filtered by rating
     *
     * @param rating the rating value by which the filter will be made
     * @return movie details json as string
     */
    @GetMapping(path = GET_MOVIE_DETAILS_BY_RATING, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<MovieDetail> getListMovieDetailsByRating(@PathVariable final int rating) {
        return movieDetailService.getListMovieDetailsByRating(rating);
    }

    /**
     * This method is used to retrieve all data movie details filtered by year
     *
     * @param year the year by which the filter will be made
     * @return movie details json as string
     */
    @GetMapping(path = GET_MOVIE_DETAILS_BY_YEAR, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<MovieDetail> getListMovieDetailsByYear(@PathVariable final int year) {
        return movieDetailService.getListMovieDetailsByYear(year);
    }

    /**
     * This method is used to retrieve all data movie details with year bigger than a given year
     *
     * @param year the year by which the filter will be made
     * @return movie details json as string
     */
    @GetMapping(path = GET_MOVIE_DETAILS_BY_YEAR_BIGGER_THAN, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<MovieDetail> getListMovieDetailsByYearBiggerThan(@PathVariable final int year) {
        return movieDetailService.getListMovieDetailsByYearBiggerThan(year);
    }
}
