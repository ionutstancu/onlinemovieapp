package mind.geek.service.implementation;

import com.fasterxml.jackson.core.JsonProcessingException;
import mind.geek.cacheLoader.CacheLoader;
import mind.geek.helper.JsonUtils;
import mind.geek.model.MovieDetail;
import mind.geek.service.MovieDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class MovieDetailServiceImpl implements MovieDetailService {

    @Autowired
    private CacheLoader cacheLoader;

    /**
     * This method is used to retrieve all data movie details
     *
     * @throws JsonProcessingException - see {@link JsonProcessingException}
     * @return movie details json as string
     * */
    @Override
    public String getAllMoviesDetails() throws JsonProcessingException {
        return JsonUtils.toJsonString(cacheLoader.getMovieDetailList());
    }

    /**
     * This method is used to retrieve all data movie details filtered by genres
     *
     * @param genresList the list of genres by which the filter will be made
     * @return movie details json as string
     * */
    @Override
    public List<MovieDetail> getListMovieDetailsByGenres(List<String> genresList) {
        List<MovieDetail> movieDetailList = new ArrayList<>();

        cacheLoader.getMovieDetailList().forEach(movieDetail ->
                genresList.stream()
                        .filter(genre -> null != movieDetail.getGenres() && movieDetail.getGenres().contains(genre))
                        .map(genre -> movieDetail).forEach(movieDetailList::add));

        return movieDetailList;
    }

    /**
     * This method is used to retrieve all data movie details filtered by director names
     *
     * @param directorsList the list of director names by which the filter will be made
     * @return movie details json as string
     * */
    @Override
    public List<MovieDetail> getListMovieDetailsByDirectors(List<String> directorsList) {
        List<MovieDetail> movieDetailList = new ArrayList<>();

        cacheLoader.getMovieDetailList().forEach(movieDetail ->
                directorsList.forEach(s ->
                        movieDetail.getDirectors().stream()
                                .filter(director -> s.contains(director.getName()))
                                .map(director -> movieDetail)
                                .forEach(movieDetailList::add)));

        return movieDetailList;
    }

    /**
     * This method is used to retrieve all data movie details filtered by rating
     *
     * @param rating the rating value by which the filter will be made
     * @return movie details json as string
     * */
    @Override
    public List<MovieDetail> getListMovieDetailsByRating(Integer rating) {
        return cacheLoader.getMovieDetailList()
                .stream()
                .filter(movieDetail -> ((null!= movieDetail.getRating()) && movieDetail.getRating().equals(rating)))
                .collect(Collectors.toList());
    }

    /**
     * This method is used to retrieve all data movie details filtered by year
     *
     * @param year the year by which the filter will be made
     * @return movie details json as string
     * */
    @Override
    public List<MovieDetail> getListMovieDetailsByYear(Integer year) {
        return cacheLoader.getMovieDetailList()
                .stream()
                .filter(movieDetail -> ((null!= movieDetail.getYear()) && movieDetail.getYear().equals(year)))
                .collect(Collectors.toList());
    }

    /**
     * This method is used to retrieve all data movie details filtered by year
     *
     * @param year the year by which the filter will be made
     * @return movie details json as string
     * */
    @Override
    public List<MovieDetail> getListMovieDetailsByYearBiggerThan(Integer year) {
        return cacheLoader.getMovieDetailList()
                .stream()
                .filter(movieDetail -> ((null!= movieDetail.getYear()) && (movieDetail.getYear() > year)))
                .collect(Collectors.toList());
    }
}
