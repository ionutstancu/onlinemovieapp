package mind.geek.service;


import com.fasterxml.jackson.core.JsonProcessingException;
import mind.geek.model.MovieDetail;

import java.util.List;

public interface MovieDetailService {
    /**
     * This method is used to retrieve all data movie details
     *
     * @throws JsonProcessingException - see {@link JsonProcessingException}
     * @return movie details json as string
     * */
    String getAllMoviesDetails() throws JsonProcessingException;

    /**
     * This method is used to retrieve all data movie details filtered by genres
     *
     * @param genresList the list of genres by which the filter will be made
     * @return movie details json as string
     * */
    List<MovieDetail> getListMovieDetailsByGenres(List<String> genresList);

    /**
     * This method is used to retrieve all data movie details filtered by director names
     *
     * @param directorsList the list of director names by which the filter will be made
     * @return movie details json as string
     * */
    List<MovieDetail> getListMovieDetailsByDirectors(List<String> directorsList);

    /**
     * This method is used to retrieve all data movie details filtered by rating
     *
     * @param rating the rating value by which the filter will be made
     * @return movie details json as string
     * */
    List<MovieDetail> getListMovieDetailsByRating(Integer rating);

    /**
     * This method is used to retrieve all data movie details filtered by year
     *
     * @param year the year by which the filter will be made
     * @return movie details json as string
     * */
    List<MovieDetail> getListMovieDetailsByYear(Integer year);

    /**
     * This method is used to retrieve all data movie details filtered by year
     *
     * @param year the year by which the filter will be made
     * @return movie details json as string
     * */
    List<MovieDetail> getListMovieDetailsByYearBiggerThan(Integer year);
}
